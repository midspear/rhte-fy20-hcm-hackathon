![cost](img/cost.jpg)

# Cost Management

# How can I access the app / environment?

Please, ask any of your hackathon leaders for the credentials to access the environment.

# How can I access the API? 

You need to have a valid username and password to access to cloud.redhat.com and this user must have the correct entitlements attached. 

If you want to get your credentials to continue working on this after the hackathon, please reach [Victor Estival](mailto:vestival@redhat.com), [Sergio Ocon](socon@redhat.com) or [Steve Fulmer](sfulmer@redhat.com)

Once you have validate your user, you can reach the API at  
[https://cloud.redhat.com/api/cost-management/v1/](https://cloud.redhat.com/api/cost-management/v1/)

# API Documentation

Where can I find the doc for the API? It is documented using [Open API](https://www.openapis.org/). You can find the full documentation [here](https://github.com/project-koku/koku/blob/master/docs/source/specs/openapi.json)

You can take that file and pop it in [https://editor.swagger.io/](https://editor.swagger.io/) to make it much more convenient to browse.



# Tracks
[API harvesting for meaningful outputs](track1.md)

[Scripting for reporting](track2.md)

[Generate Services around the product](track3.md)

[Mock review / feedback](track4.md)

You are welcome to create you own track, please talk to any of the Hackathon leaders about it and submit a PR 

# Feedback

We would like you to provide constructive feedback about the tool, your experience and any possible improvements via the provided [form]()