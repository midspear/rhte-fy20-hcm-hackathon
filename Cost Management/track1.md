# API harvesting for meaningful outputs
In this track we do expect the participants to explore the API and create meaningful outputs, like for intance

`https://cloud.redhat.com/api/cost-management/v1/reports/aws/costs/?delta=cost&filter[limit]=10&filter[resolution]=monthly&filter[time_scope_units]=month&filter[time_scope_value]=-2&group_by[tag:Stack]=project%20ocp4-&order_by[cost]=desc`
 
`https://cloud.redhat.com/api/cost-management/v1/reports/aws/costs/?delta=cost&filter[limit]=10&filter[resolution]=monthly&filter[time_scope_units]=month&filter[time_scope_value]=-2&group_by[tag:Stack]=project%20ocp-clientvm&order_by[cost]=desc`


`https://cloud.redhat.com/api/cost-management/v1/reports/aws/costs/?delta=cost&filter[limit]=10&filter[resolution]=monthly&filter[time_scope_units]=month&filter[time_scope_value]=-2&group_by[tag:Stack]=project%20ocp-clientvm&order_by[cost]=desc`