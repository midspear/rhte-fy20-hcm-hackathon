# Mock review / feedback

During this session we will review and discuss, looking for feedback our Overview page.

The kind of feedback we're looking for is:

* Information shown
* Outline
* Expectations
* General user experience
* How would you evolve the dashborads
* Look & Feel

Please use the [following mockups](placeholder_for_marvelapp_link) as a base for the discussion

To provide a consistant and meaningful feedback please use this [Google Form]()