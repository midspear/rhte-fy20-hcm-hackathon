![lock](img/lock.png)

# Catalog

# How can I access the app / environment?

Please, ask any of your hackathon leaders for the credentials to access the environment.

# How can I access the API? 

You need to have a valid username and password to access to cloud.redhat.com and this user must have the correct entitlements attached. 

If you want to get your credentials to continue working on this after the hackathon, please reach [Victor Estival](mailto:vestival@redhat.com), [John Hardy](jhardy@redhat.com) or [Steve Fulmer](sfulmer@redhat.com)

Once you have validate your user, you can reach the API at  
[https://cloud.redhat.com/api/catalog/v1.0/](https://cloud.redhat.com/api/catalog/v1.0/)

# API Documentation

Where can I find the doc for the API? It is documented using [Open API](https://www.openapis.org/). You can find the full documentation [here](https://cloud.redhat.com/api/catalog/v1.0/openapi.json)



# API Clients
We would expect most users to build a client.  we maintain repos for the ones that dev uses ([created by (Open API Generator](https://openapi-generator.tech/))

* [https://github.com/ManageIQ/catalog-api-jsclient](https://github.com/ManageIQ/catalog-api-jsclient) 
* [https://github.com/ManageIQ/approval-api-client-ruby](https://github.com/ManageIQ/approval-api-client-ruby)
* [https://github.com/ManageIQ/approval-api-jsclient](ttps://github.com/ManageIQ/approval-api-jsclient)

# Tracks
[Approval API](track1.md)