![coding](img/coding.jpg)

# Hybrid Cloud Management (HCM) Hackathon

It does not matter what your role is at Red Hat, everyone needs to know about Hybrid Cloud Management in a hands on fashion. During this session, attendees will have the opportunity to explore Cost Management, Catalog, and CloudForms. Attendees will learn how to use APIs to generate outputs, Scripting for Reporting, extending Cloudforms with Ansible. The goal is to push attendees to their limits exploring APIs, new features and capabilities. At the same time, gather concrete data on the new capabilities of the Hybrid Cloud Management suite of products. 


Thanks for joining to this Hackathon. If you want to know more about what a hackathon is, please refer to our presentation [here](https://docs.google.com/presentation/d/1AdZzq92lGTcd9uqv6_g0uPnr70vYFbcr08VRhT3Tr3U/edit#slide=id.g5c7ba2e508_0_200)

## Important!

 ***Please note that we will use the same repo and tracks in all three GEOs: APAC, EMEA and NA, the goal is to continue the hackathon over the RHTE*** 
 
# Official hackathon definition

A hackathon (also known as a hack day, hackfest or codefest) is a design sprint-like event in which computer programmers and others involved in software development, including graphic designers, interface designers, project managers, and others, often including subject-matter-experts, collaborate intensively on software projects.

Souce: https://en.wikipedia.org/wiki/Hackathon

# HCM hackathon definition

A HCM Hackathon is a few hours event where product experts from all Red Hat departments and Partners work together and collaborate on HCM solution: CloudForms, Cost Management and Catalog to create API example, Automate Code, Ansible Playbooks, Reports, Policies, Demos, Guides, Blog Posts or any other content related to the product.

Source: Christian Jung / COPs


# What to expect?

You are expected to expand and explore current product capabilities. You can enroll to any of the tracks (due to the short amount of time we have we don't believe it is a good idea to be swaping tracks but you can do it if that is what you really want) 

# Generic API information about cloud.redhat.com
You can find generic information about the API services available at https://cloud.redhat.com/api/

#Tools
We do expect all the output into gitlab, you can use any other took, like google docs or github but you must add a link to your progress into gitlab: https://gitlab.com/redhat-cop/rhte-fy20-hcm-hackathon

# Where can I find the tracks?
You can find the tracks inside each product's folder

[CloudForms](CloudForms/README.md)

[Cost Management](Cost Management/README.md)

[Catalog](Catalog/README.md)

Image by <a href="https://pixabay.com/users/Pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1841550">Pexels</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1841550">Pixabay</a>

Sortlink to this page: http://bit.ly/rhte19hack