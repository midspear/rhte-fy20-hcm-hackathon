# Reports, Control Policies, Alerts - build and document examples

We would like to curate and add more content to CloudForms so we have more 

* Reports
* Dashboads
* Policies
* Policiy Profiles
* Alerts
* Alert profiles

And we can add them to the product. 

Many people was using [Ramrexx ones](https://github.com/ramrexx?tab=repositories) but those are not longer maintained by Kevin 

We have a bunch already in the [COP gitlab repo](https://gitlab.com/redhat-cop/mbu-lab/cloudforms)

You can find more information about this [here](https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/introduction/issues/33) from previous hackathons