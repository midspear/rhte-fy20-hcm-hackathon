![cloud-computing](img/cloud-computing.jpg)

# CloudForms

# How can I access the app / environment?

Please, ask any of your hackathon leaders for the credentials to access the environment.

# How can I access the API?

CloudForms API are accessed using /api at the end of your login URL. Please note that you need to supply your credentials again. 

# API Documentation

You can find the API documentation at 

* https://access.redhat.com/documentation/en-us/red_hat_cloudforms/4.7/html/red_hat_cloudforms_rest_api/index
* http://manageiq.org/docs/reference/fine/api/examples/queries
* http://manageiq.org/docs/reference/euwe/doc-REST_API/miq/index

# Tracks

[Runbook for demos](track1.md)

[Reports, Control Policies, Alerts - build and document examples](track2.md)

[Extend CloudForms capabilities via Ansible Playbooks](track3.md)

[Working session about the new policies editor and task  / jobs displaying](track4.md)

[Build workshops](track5.md) 

You are welcome to create you own track, please talk to any of the Hackathon leaders about it and submit a PR 

# Feedback

We would like you to provide constructive feedback about the tool, your experience and any possible improvements via the provided [form]()